package com.groundgurus.day4;

import java.util.HashSet;

public class SetExample {
    public static void main(String[] args) {
        var mySet = new HashSet<>();
        mySet.add("Hello");
        mySet.add("Hi");
        mySet.add("Ha");
//        mySet.add("Hello");

        System.out.println(mySet);
    }
}

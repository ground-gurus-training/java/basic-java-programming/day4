package com.groundgurus.day4;

import java.util.HashMap;

public class MapExample {
    public static void main(String[] args) {
        var foods = new HashMap<>();
        foods.put("A", "\uD83C\uDF4E");
        foods.put("B", "\uD83C\uDF4C");
        foods.put("C", "\uD83C\uDF3D");

        System.out.println(foods.get("C"));
    }
}

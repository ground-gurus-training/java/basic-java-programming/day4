package com.groundgurus.day4.generics;

public interface Mapping<K, E> {
    E toEmoji(K k);

    K toKey(E v);
}

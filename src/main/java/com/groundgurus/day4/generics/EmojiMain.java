package com.groundgurus.day4.generics;

public class EmojiMain {
    public static void main(String[] args) {
        var emojiMapping = new EmojiMapping();
        emojiMapping.init();

        var emoji = emojiMapping.toEmoji(1);
        System.out.println(emoji);

        var key = emojiMapping.toKey("\uD83C\uDF3D");
        System.out.println(key);
    }
}

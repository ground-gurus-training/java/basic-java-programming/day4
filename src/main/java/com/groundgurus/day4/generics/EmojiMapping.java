package com.groundgurus.day4.generics;

import java.util.HashMap;
import java.util.Map;

public class EmojiMapping implements Mapping<Integer, String> {
    Map<Integer, String> emojis = new HashMap<>();

    public void init() {
        emojis.put(1, "\uD83C\uDF4E");
        emojis.put(2, "\uD83C\uDF4C");
        emojis.put(3, "\uD83C\uDF3D");
    }

    @Override
    public String toEmoji(Integer key) {
        return emojis.get(key);
    }

    @Override
    public Integer toKey(String emoji) {
        var keys = emojis.keySet();
        for (Integer key : keys) {
            if (emojis.get(key).equals(emoji)) {
                return key;
            }
        }

        return -1;
    }
}

package com.groundgurus.day4;

import java.util.ArrayList;

public class ListExample {
    public static void main(String[] args) {
        var myList = new ArrayList<>();
        myList.add("Hello");
        myList.add("Hi");
        myList.add("Howdy");
        myList.add("Hi");
        System.out.println(myList);
        System.out.println(myList.get(1));
    }
}

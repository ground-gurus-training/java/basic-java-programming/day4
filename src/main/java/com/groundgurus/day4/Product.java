package com.groundgurus.day4;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Product {
    private String name;
    private String manufacturer;

//    @Override
//    public boolean equals(Object obj) {
//        if (obj instanceof Product otherProduct) {
//            return otherProduct.name.equals(name) && otherProduct.manufacturer.equals(manufacturer);
//        }
//
//        return false;
//    }
}

package com.groundgurus.day4;

public class ProductMain {
    public static void main(String[] args) {
        var gatorade = new Product("Gatorade", "Pepsi Philippines");
        var gatorade2 = new Product("Gatorade", "Pepsi Philippines");
        var cobra = new Product("Cobra Energy Drink", "ACME");

        System.out.println(gatorade.equals(gatorade2));
        System.out.println(gatorade == cobra);

        System.out.println(gatorade.getClass().getName());
        System.out.println(gatorade.getClass().getSimpleName());
    }
}

package com.groundgurus.day4.exercises;

public class Day4Exercise1 {
    public static void main(String[] args) {
        var value = "Public Relations Agent Asks Sports Room, When Do Television Disc Jockeys Fight?";
        var output = value.toLowerCase();
        System.out.println(output);
    }
}

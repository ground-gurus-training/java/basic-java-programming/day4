package com.groundgurus.day4.exercises;

import java.util.Scanner;

public class Day4Exercise2 {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        var value = "Being bounced around quickly annoyed the disheveled taxi drivers";

        System.out.print("Enter string to find: ");
        var toFind = scanner.next();

        System.out.print("Enter string to replace: ");
        var toReplace = scanner.next();

        value = value.replace(toFind, toReplace);

        System.out.println(value);
    }
}

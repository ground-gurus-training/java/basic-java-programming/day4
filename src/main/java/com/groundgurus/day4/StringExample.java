package com.groundgurus.day4;

public class StringExample {
    public static void main(String[] args) {
        var presentationLine = "You're breathtaking!";

        System.out.println(presentationLine.replace("!", "?"));
    }
}

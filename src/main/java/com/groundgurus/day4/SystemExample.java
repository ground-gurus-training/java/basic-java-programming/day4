package com.groundgurus.day4;

import java.io.IOException;

public class SystemExample {
    public static void main(String[] args) throws IOException {
//        var properties = System.getProperties();
//        properties.list(System.out);
        var userHome = System.getProperty("path.separator");
        System.out.println(userHome);
    }
}

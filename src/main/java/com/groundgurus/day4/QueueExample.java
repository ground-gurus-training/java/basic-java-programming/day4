package com.groundgurus.day4;

import java.util.PriorityQueue;

public class QueueExample {
    public static void main(String[] args) {
        var myQueue = new PriorityQueue<>();
        myQueue.add("First");
        myQueue.add("Second");
        myQueue.add("Third");
        myQueue.remove();

        System.out.println(myQueue);
    }
}

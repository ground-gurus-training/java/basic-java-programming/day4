package com.groundgurus.day4;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class FormatExample {
    public static void main(String[] args) {
        var locale = new Locale("tl", "Philippines");
        var df = DateFormat.getDateTimeInstance(
                DateFormat.LONG, DateFormat.SHORT, locale);
        String today = df.format(new Date());
        System.out.println(today);
    }
}
